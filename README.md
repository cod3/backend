
#Proyecto BackEnd en Django

## Como iniciar proyecto

```
git clone https://ramborg@bitbucket.org/cod3/backend.git
cd backend
pip install -r requeriments.txt
```

####Instalar en VirtualEnv

```
virtualenv venv
source venv/bin/activate
```

## Como ejecutar proyecto benviu

```
cd benviu_proj
python manage.py runserver
```

http://127.0.0.1:8000

## Acceso a Administración

http://127.0.0.1:8000/admin/

User: admin
Pass: cod3

## Acceso a API

http://127.0.0.1:8000/benviu/api

## Acceso a la BD

####Iniciar BD

```
python manage.py syncdb
```
Ver que SQLs ha ejecutado
```
python manage.py sql rango
```

####Access shell y BD

```
python manage.py shell
```

```python
# Import the Category model from the Rango application
>>> from rango.models import Category

# Show all the current categories
>>> print Category.objects.all()
[] # Returns an empty list (no categories have been defined!)

# Create a new category object, and save it to the database.
>>> c = Category(name="Test")
>>> c.save()

# Now list all the category objects stored once more.
>>> print Category.objects.all()
[<Category: test>] # We now have a category called 'test' saved in the database!

# Quit the Django shell.
>>> quit()
```


------------------------
Guia de Markdown: https://guides.github.com/features/mastering-markdown/#syntax
