# -*- encoding: utf-8 -*-


import os

def populate():
    python_cat = add_group('Amy Winehouse ')

    add_local(cat=python_cat,
        name="Continental",
        description = "Bar músical para tomar cocteles.",
        web_page="")

    add_local(cat=python_cat,
        name="Insòlita Gea",
        description = "Bar músical para tomar cocteles.",
        address = "Calle de Sant Antoni Maria Claret, 216",
        province = "Barcelona",
        postal_code = "08025",
        
        phone_number = "+34 934 33 57 61",
        lat_position = 41.411282,
        long_position = 2.174612,
        
        web_page = "")

    add_local(cat=python_cat,
        name="Cara B")
        
    django_cat = add_group("Grupo8")

    add_local(cat=django_cat,
        name="Coleccionista")

    add_local(cat=django_cat,
        name="Lunatic Pub")

    add_local(cat=django_cat,
        name="Big Ben")

    frame_cat = add_group("Bardicada")


    # Print out what we have added to the user.
    for c in Local.objects.all():
      print c
    
 
def add_local(cat, name, description="Bar músical", address="", province="", postal_code="", lat_position=0, long_position=0, phone_number="", web_page="", views=0):
    p = Local.objects.get_or_create(name=name, address=address, province=province, postal_code=postal_code, lat_position=lat_position, long_position=long_position, phone_number=phone_number, web_page=web_page,views=views)[0]
    return p

def add_group(name):
    g = Grupo.objects.get_or_create(name=name, description="Grupo músical")[0]
    return g

# Start execution here!
if __name__ == '__main__':
    print "Starting Rango population script..."
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'benviu_proj.settings')
    from benviu.models import Grupo, Local
    populate()

