from django.shortcuts import render

# Create your views here.

'''
# Respuesta simple
from django.http import HttpResponse
return HttpResponse("Rango says hello world!")
'''
from django.conf import settings
from django.template import RequestContext
from django.shortcuts import render_to_response

from benviu.models import Local, Grupo, Evento

# API imports
from rest_framework import viewsets
from benviu.serializers import LocalSerializer, GrupoSerializer, EventoSerializer

def index(request):
  
  
    print settings.BASE_DIR
    print settings.SETTINGS_DIR
    print settings.PROJECT_PATH
    print settings.TEMPLATE_PATH
    
    # Request the context of the request.
    # The context contains information such as the client's machine details, for example.
    context = RequestContext(request)

    local_list = Local.objects.order_by('-likes')[:5]
    context_dict = {'locales': local_list}

    # Render the response and send it back!    
    return render_to_response('benviu/index.html', context_dict, context)
    

#############
# API VIEWS #
#############
# class LocalView(generics.ListAPIView):
class LocalView(viewsets.ModelViewSet):
    """
    Returns a list of all authors.
    """
    model = Local
    serializer_class = LocalSerializer
    
class GrupoView(viewsets.ModelViewSet):
    """
    Returns a list of all authors.
    """
    model = Grupo
    serializer_class = GrupoSerializer
    
class EventoView(viewsets.ModelViewSet):
    """
    Returns a list of all authors.
    """
    model = Evento
    serializer_class = EventoSerializer
    
