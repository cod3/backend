from django.conf.urls import patterns, url, include
from benviu import views

from benviu.models import Local, Grupo, Evento
from rest_framework import viewsets, routers

# ViewSets define the view behavior.
class LocalViewSet(viewsets.ModelViewSet):
    model = Local

class GrupoViewSet(viewsets.ModelViewSet):
    model = Grupo
    
class EventoViewSet(viewsets.ModelViewSet):
    model = Evento
    
# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'locales', LocalViewSet)
router.register(r'grupos', GrupoViewSet)
router.register(r'eventos', EventoViewSet)

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
        
    url(r'^api/', include(router.urls)),
    url(r'^api/locales/$', views.LocalView.as_view(), name='local-list'),
    url(r'^api/grupos/$', views.GrupoView.as_view(), name='grupo-list'),
    url(r'^api/eventos/$', views.EventoView.as_view(), name='evento-list'),
)
