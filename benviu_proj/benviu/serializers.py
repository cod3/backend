from django.contrib.auth.models import User, Group
from benviu.models import Local, Grupo, Evento
from rest_framework import serializers

class LocalSerializer(serializers.ModelSerializer):
    """
    Serializing all the Locales
    """
    class Meta:
        model = Local
        fields = ('name', 'description', 'web_page', 'email', 'phone_number', 'address', 'postal_code', 
                  'province', 'lat_position', 'long_position', 'likes', 'views')

class GrupoSerializer(serializers.ModelSerializer):
    """
    Serializing all the Locales
    """
    class Meta:
        model = Grupo
        fields = ('name', 'description', 'tags', 'web_page', 'email', 'phone_number', 'likes', 'views')

class EventoSerializer(serializers.ModelSerializer):
    """
    Serializing all the Locales
    """
    class Meta:
        model = Evento
        '''
        fields = ('title', 'description', 'web_page', 'email', 'phone_number', 'address', 'postal_code', 
                  'province', 'lat_position', 'long_position', 'likes', 'views')
        '''
