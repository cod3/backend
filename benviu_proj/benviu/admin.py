from django.contrib import admin

from benviu.models import Grupo, Local, Evento

admin.site.register(Grupo)
admin.site.register(Local)
admin.site.register(Evento)
