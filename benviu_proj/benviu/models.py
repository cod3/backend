# -*- encoding: utf-8 -*-

from django.db import models

# Create your models here.

'''
Clase representa Grupo musica

Con Nombre, Descripcion, Genero/Tags, Pagina web, Avaar/Fotos
'''
class Grupo(models.Model):
    # Nombres
    name = models.CharField(max_length=128) #, unique=True
    description = models.CharField(max_length=255)
    
    tags = models.CharField(max_length=255)
    
    # Contacto
    web_page = models.URLField()
    email = models.EmailField()
    phone_number = models.CharField(max_length=15)
    
    likes = models.IntegerField(default=0)
    views = models.IntegerField(default=0)


    def __unicode__(self):
        return self.name

'''
Clase que representa un Bar/Local

Alberga Nombre
        Direccion(municipio,cp,calle,num,latitud,long)
        Horario, Tipo bar, Pagina web
'''
class Local(models.Model):
    # Nombres
    name = models.CharField(max_length=128) #, unique=True
    description = models.CharField(max_length=255)
    # Contacto
    web_page = models.URLField()
    email = models.EmailField()
    phone_number = models.CharField(max_length=15)
    # Dirección
    address = models.CharField(max_length=255)
    postal_code = models.CharField(max_length=10)
    province = models.CharField(max_length=128)
    lat_position = models.DecimalField (max_digits=8, decimal_places=3)
    long_position = models.DecimalField (max_digits=8, decimal_places=3)
    
    
    likes = models.IntegerField(default=0)
    views = models.IntegerField(default=0)


    def __unicode__(self):
        return self.name

'''
Clase representa Evento

Con Fecha, Hora, Precio, Grupo y Local, Descripcion/Tags
'''
class Evento(models.Model):
    # Relaciones
    grupo = models.ForeignKey(Grupo)
    local = models.ForeignKey(Local)
    
    # Nombre
    title = models.CharField(max_length=128)
    description = models.CharField(max_length=255)
    tags = models.CharField(max_length=255)
    
    # Fecha y lugar
    date_time = models.DateTimeField()
    lat_position = models.DecimalField (max_digits=8, decimal_places=3)
    long_position = models.DecimalField (max_digits=8, decimal_places=3)
    
    views = models.IntegerField(default=0)

    def __unicode__(self):
        return self.title
